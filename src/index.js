import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import VocabApplet from './js/App';
import registerServiceWorker from './js/registerServiceWorker';

ReactDOM.render(<VocabApplet />, document.getElementById('root'));
registerServiceWorker();
