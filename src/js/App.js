import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import ListPage from './ListPage';
import Home from './Home';
import VocabPage from './VocabPage';
import Quiz from './Quiz';
import QuizSummary from './QuizSummary';

class VocabApplet extends React.Component {
    //TODO: Rethink state after bringing auth into this.
    constructor(props) {
        super(props);
        this.state = {
            quizQuestionCount: 20,
            quizQuestionTimed: true,
            quizSummary: [],
        };
        this.updateSettings = this.updateSettings.bind(this);
        this.updateQuizResult = this.updateQuizResult.bind(this);
    };

    updateSettings ( count, timed ) {
        console.log(timed)
        this.setState({
            quizQuestionCount: count,
            quizQuestionTimed: timed,
        });
    }

    updateQuizResult ( result ) {
        this.setState({
            quizSummary: result,
        });
    }

    /*
        TODO: Config for quiz should probably belong to each list after auth but have defaults.
              For this demo project, since no persistence is implemented, config will be toplevel.
    */
    render() {
        return (
            <Router>
                <div className="VocabApplet">
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/vocablists" component={ListPage}/>
                    <Route exact path={"/vocablists/:listName"} 
                           render={(props) => <VocabPage {...props} 
                                                         changeSettings={this.updateSettings} 
                                                         timed={this.state.quizQuestionTimed}
                                                         qCount={this.state.quizQuestionCount}/>}/>
                    <Route exact path={"/vocablists/:listName/quiz"} 
                           render={(props) => <Quiz {...props} 
                                                     qCount={this.state.quizQuestionCount} 
                                                     timed={this.state.quizQuestionTimed}
                                                     sendResults={this.updateQuizResult}/>}/>
                    <Route exact path={"/vocablists/:listName/quiz/results"} 
                           render={(props) => <QuizSummary {...props} 
                                                           isTimed={this.state.quizQuestionTimed}
                                                           summary={this.state.quizSummary}/>}/>                                 
                </div>
            </Router>
        );
    };
}

export default VocabApplet;
