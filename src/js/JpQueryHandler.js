/* 
    Handles JP Alphabet Queries
*/
class JpQueryHandler {
    checkKana ( str ) {
        return (str.charCodeAt(0) >= 0x3040 && str.charCodeAt(0) <= 0x30ff) ? true : false;
    }
    checkIndex( ch ) {
        var buf = ch.charCodeAt(0);
        if (ch.charCodeAt(0) >= 0x30a0){
            buf = ch.charCodeAt(0) - 0x0060;
        }
        if (buf >= 0x3041 && buf < 0x304b){
            return "あ";
        }
        if (buf >= 0x304b && buf < 0x3055){
            return "か";
        }
        if (buf >= 0x3055 && buf < 0x305f){
            return "さ";
        }
        if (buf >= 0x305f && buf < 0x306a){
            return "た";
        }
        if (buf >= 0x306a && buf < 0x306f){
            return "な";
        }
        if (buf >= 0x306f && buf < 0x307e){
            return "は";
        }
        if (buf >= 0x307e && buf < 0x3083){
            return "ま";
        }
        if (buf >= 0x3083 && buf < 0x3089){
            return "や";
        }
        if (buf >= 0x3089 && buf < 0x308e){
            return "ら";
        }
        if (buf >= 0x308e && buf < 0x3094){
            return "わ";
        }
        return "";
    }
}

export let jpQueryHandler = new JpQueryHandler();