import React from 'react';
import '../styles/quiztimer.css'

//this class will use setInterval, as pinpoint accuracy within the 30 seconds is not really a huge concern.  this may well change later as the app grows
class QuizTimer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            timeSecs: 10,
        };
        this._timer = null;

        this._downtick = this._downtick.bind(this);
    };

    //TODO: miteirudoori yo.
    setTime(){

    }

    reset() {
        if (this.props.active){
            if (this._timer) {
                clearInterval(this._timer);
                this._timer = null;
                this.setState({timeSecs: 10});
            }
        }
    }

    start() {
        if (this.props.active){
            if (this._timer === null) {
                this._timer = setInterval(this._downtick, 1000);
            }
        }
    }

    _downtick(){
        let tickedTime = this.state.timeSecs - 1;
        this.setState({
          timeSecs: tickedTime,
        });
        
        // Check if we're at zero.
        if (tickedTime === 0) { 
          this.reset();
          this.props.callBackTimeOut();
        }
    }

    render() {
        return (
            <div id="quiz-timer">
                <span className={"centre " + (this.props.active ? "" : "swappedOut")}>
                    {this.state.timeSecs}
                </span>
            </div>
        );
    };
}

export default QuizTimer;
