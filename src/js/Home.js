import React from 'react';
import '../styles/home.css';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        //this.changeStage = this.changeStage.bind(this);
    };

    createRouteButton( name, route) {
        return (
            <button type="button" 
                    className="btn btn-outline-secondary"
                    onClick={() => { this.props.history.push(route) }}>
                {name}
            </button> 
        );
    };

    render() {
        return (
            <div id="home" className="d-flex flex-column align-items-center">
                <div className="centre title" id="title">
                    Welcome to the Vocab Applet.
                </div>
                <div className="explanation">
                    This is an alpha UI for a quiz applet.
                </div>
                <div className="explanation">    
                    For now, only JLPT lists will be available.
                </div>
                <div className="explanation">    
                    Many more features are coming through soon.
                </div>
                <br/><br/>
                <div className="d-flex flex-column align-items-stretch">
                    <button type="button" 
                            className="btn btn-outline-secondary"
                            onClick={() => { this.props.history.push("/vocablists") }}>
                        Vocab Lists
                    </button> 
                    <br/>
                    <button type="button" className="btn btn-outline-secondary" disabled>
                        Assess
                    </button>
                    <br/>
                    <button type="button" className="btn btn-outline-secondary" disabled>
                        Register
                    </button>
                </div>
            </div>
        );
    };
}

export default Home;
