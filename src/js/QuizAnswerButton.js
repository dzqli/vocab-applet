import React from 'react';
import '../styles/quizanswerbutton.css'

//this class could become a lot more complex based on handling user data / questions statistics
class QuizAnswerButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    };

    render() {
        return (
            <div className={"col-md-5 quiz-button " + 
                            ((this.props.userClicked !== null || this.props.timedOut) 
                                && this.props.correct ? "correct " : "") + 
                            ((this.props.userClicked === this.props.index || 
                                this.props.timedOut) && !this.props.correct ? "incorrect " : "") +
                            (this.props.userClicked !== null || this.props.timedOut ? "clicked " : "")}
                 onClick={()=>this.props.callBackAnswer(this.props.index)}>
                {this.props.answerText}
            </div>
        );
    };
}

export default QuizAnswerButton;
