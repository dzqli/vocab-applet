import React from 'react';
import '../styles/listtile.css'

class ListTile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        //this.changeStage = this.changeStage.bind(this);
    };

    render() {
        return (
            <div id={"list-"+this.props.title} 
                 className="list-tile"
                 onClick={() => this.props.navFunc('/'+this.props.title)}>
                <img className="list-tile-img" src={this.props.BG} alt="TileBG"/>
                <div className="tile-overlay d-flex flex-column">
                    <div className="list-tile-title">{this.props.title}</div>
                    <div className="list-tile-description">{this.props.description}</div>
                </div>
            </div>
        );
    };
}
export default ListTile;