import React from 'react';
import { listData } from './ListData';
import VocabTile from './VocabTile';
import { jpQueryHandler } from './JpQueryHandler';
import '../styles/vocabpage.css';
import SettingsLogo from '../assets/settingslogo.svg';

class VocabPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            vocab: [],
            settingsOpen: false,
            countSettingError: false,
            settingsSaved: "",
        };
        this.refCount = React.createRef();
        this.refTimed = React.createRef();
        this.valueChanged = this.valueChanged.bind(this);

        //this.changeStage = this.changeStage.bind(this);
    };

    //TODO: Probe for these in component through api calls, and generisize cards.
    //TODO: figure out large list loading procedure
    componentDidMount() {
        this.setState({
            vocab: listData.getList(this.props.match.params.listName),
        });
    } 

    validateAndSaveSettings(){
        this.setState()
        if (this.refCount.current.value < 1 || this.refCount.current.value > 50){
            this.setState({countSettingError: true, settingsSaved: ""});
        }
        else {
            this.props.changeSettings( this.refCount.current.value, this.refTimed.current.checked );
            this.setState({countSettingError: false, settingsSaved: true});
        }
    }

    valueChanged() {
        this.setState({
            countSettingError: false,
            settingsSaved: false,
        });
    }

    //TODO: Ensure sorted order
    renderTiles() {
        var tileMarkup = [];
        var currentInitial = "";

        for (var i = 0; i < this.state.vocab.length; i++){
            var buf = "";
            if (jpQueryHandler.checkKana(this.state.vocab[i].Kanji)){
                buf = this.state.vocab[i].Kanji;
            }
            else {
                buf = this.state.vocab[i].Hiragana;
            }
            if (jpQueryHandler.checkIndex(buf.charAt(0)) !== currentInitial){
                currentInitial = jpQueryHandler.checkIndex(buf.charAt(0));
                tileMarkup.push(<div className="title w-100"
                                     key={currentInitial+i}>
                                    {currentInitial}
                                </div>);
            }
            tileMarkup.push(<VocabTile key={"vocab" + i}
                                       kanji={this.state.vocab[i].Kanji}
                                       hiragana={this.state.vocab[i].Hiragana}
                                       english={this.state.vocab[i].English}/>);
        }
        return tileMarkup;
    }

    //TODO: unbloat 
    render() {
        return (
            <div id="vocab-page" className="d-flex flex-column">
                <div className="centre title">
                    {this.props.match.params.listName} Vocabulary.
                </div>
                <div className="centre settings-btn" onClick={()=> this.setState({settingsOpen: !this.state.settingsOpen})}>
                    <img className="settings-icon" src={SettingsLogo} alt="Gear Icon"/>
                </div>
                <div className={"settings-wrapper container "+ (this.state.settingsOpen ? "" : "swappedOut")}>
                    <div className="row">
                        <div className="col-md-6">Number of questions per quiz: </div>
                        <input ref={this.refCount} 
                               className={"col-md-6 " + (this.state.countSettingError ? "error" : "")}
                               type="number" 
                               id="q-count" 
                               defaultValue={this.props.qCount}
                               onChange={this.valueChanged}/>
                    </div>
                    <div className="row">
                        <div className="col-md-6">Quiz is timed: </div>
                        <input ref={this.refTimed}
                               className=""
                               type="checkbox"
                               id="q-timed"
                               name="isTimed"
                               value={true}
                               defaultChecked={this.props.timed}
                               onChange={this.valueChanged}/>
                    </div>
                    <div className="row">
                        <small className={"msg-danger centre "+(this.state.countSettingError ? "" : "swappedOut")}>
                            Please enter a quiz count between 1 and 50.
                        </small>
                    </div>
                    <div className="row">
                        <small className={"msg-info centre "+(this.state.settingsSaved === false ? "" : "swappedOut")}>
                            Please press save to persist your settings.
                        </small>
                    </div>
                    <div className="row">
                        <small className={"msg-success centre "+(this.state.settingsSaved === true ? "" : "swappedOut")}>
                            Settings Saved!
                        </small>
                    </div>
                    <div className="row">
                        <button className="btn btn-outline-primary col-md-2 offset-md-5" type="button" onClick={() => this.validateAndSaveSettings()}>Save</button>
                    </div>
                </div>
                <button className="btn btn-outline-primary centre" type="button" onClick={()=>this.props.history.push(this.props.match.url+'/quiz')}>Start Quiz</button>
                <div className="vocab-tile-wrapper d-flex flex-wrap">
                    { this.renderTiles() }
                </div>
            </div>
        );
    };
}
export default VocabPage;