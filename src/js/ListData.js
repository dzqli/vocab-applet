import listN1 from '../assets/N1.json'
import listN2 from '../assets/N2.json'
import listN3 from '../assets/N3.json'
import listN4 from '../assets/N4.json'
import listN5 from '../assets/N5.json'


/* 
    proxy-database.  to be replaced by a database/api call handler
*/
class ListData {
    constructor() {
        this.lists = []
        this.lists.push({name:"JLPT N1", description: "The hardest set of JLPT Vocabulary.  Represents proficiency enough to satisfy most professonal occasions.", list: listN1});
        this.lists.push({name:"JLPT N2", description: "The advanced vocabulary of JLPT.  Represents proficiency enough to live comfortably.", list: listN2});
        this.lists.push({name:"JLPT N3", description: "The transitional vocabulary of JLPT.  Represents proficiency enough to converse semi-comfortably.", list: listN3});
        this.lists.push({name:"JLPT N4", description: "The basic vocabulary of JLPT.  Represents proficiency enough to hold a basic conversation.", list: listN4});
        this.lists.push({name:"JLPT N5", description: "The introductory vocabulary of JLPT.  Represents proficiency enough to identify basic phrases.", list: listN5});
    }
    getListNameDescription() {
        var names = [];
        for (var i = 0; i < this.lists.length; i++){
            names.push({name: this.lists[i].name, description: this.lists[i].description});
        }
        console.log(names)
        return names;
    }
    getList( name ) {
        for (var i = 0; i < this.lists.length; i++){
            if (this.lists[i].name === name){
                return this.lists[i].list;
            }
        }
    }
    //should be implemented as a dictionary / table
    //should also be relegated to backend once persistence is up
    _getRandom ( index, list, count) {
        var randoms = [];
        var hash = {};

        for (var i = 0; i < count; i++){
            var indexBuf = index;
            while (index === indexBuf || hash[indexBuf]){
                indexBuf = Math.floor(Math.random() * list.length);
            }
            hash[indexBuf] = true;
            randoms.push(list[indexBuf].English);
        }
        return randoms;
    }

    //generisize later.
    getQuestionList( numQuestions, listName ) {
        var selectedList;
        var questionsHash = {};
        var questionsOut = [];
        for (var i = 0; i < this.lists.length; i++){
            if (this.lists[i].name === listName){
                selectedList = this.lists[i].list;
            }
        }
        for (var y = 0; y < numQuestions; y++){
            var indexBuf;

            while (indexBuf === undefined || questionsHash[indexBuf]){
                indexBuf = Math.floor(Math.random() * selectedList.length);
            }
            questionsHash[indexBuf] = true;  
            
            //build right answer here 
            var answers = ["","","",""];
            var correctIndex = Math.floor(Math.random() * 4);
            answers[correctIndex] = selectedList[indexBuf].English;
            var incorrectAnswers = this._getRandom( indexBuf, selectedList, 3);
            var answersInd = 0; 
            for (var x = 0; x < incorrectAnswers.length; x++){
                while (answers[answersInd]) {
                    answersInd ++;
                }
                answers[answersInd] = incorrectAnswers[x];
            }

            questionsOut.push({
                question: ( selectedList[indexBuf].Kanji ? selectedList[indexBuf].Kanji : selectedList[indexBuf].Hiragana ),
                supplement: selectedList[indexBuf].Hiragana,
                correctIndex: correctIndex,
                answers: answers,
            });
        }
        return questionsOut;
    }
}

export let listData = new ListData();
