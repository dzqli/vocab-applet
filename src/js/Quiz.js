import React from 'react';
import { listData } from './ListData';
import QuizAnswerButton from './QuizAnswerButton';
import QuizTimer from './QuizTimer';
import '../styles/quiz.css'

class Quiz extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            questionList: listData.getQuestionList(this.props.qCount, this.props.match.params.listName),
            currentQ: 0,
            timedOut: false,
            userClicked: null,
            userStats: [],
        };
        this._refreshDelay = null;

        this.refTimer = React.createRef();
        this.callBackTimeOut = this.callBackTimeOut.bind(this);
        this.callBackAnswer = this.callBackAnswer.bind(this);
        this._renderAnswerbuttons = this._renderAnswerbuttons.bind(this);
        this._updateQuizTick = this._updateQuizTick.bind(this);
    };

    componentDidMount(){
        this.refTimer.current.start();
    }

    componentWillUnmount (prevProps) {
        if (this._refreshDelay) {
            clearTimeout(this._refreshDelay);
        }
    }

    callBackTimeOut() {
        this.setState({
            timedOut: true,
        });
        this._refreshDelay = setTimeout(this._updateQuizTick,5000);
    }

    callBackAnswer( index ) {
        if (!(this.state.userClicked || this.state.timedOut)){
            this.refTimer.current.reset();

            this.setState({
                userClicked: index,
            });
            this._refreshDelay = setTimeout(this._updateQuizTick,5000);
        }
    }

    _updateQuizTick(){
        var buf = this.state.userStats;
        var questionObj = this.state.questionList[this.state.currentQ];
        buf.push({question: questionObj.question,
                  supplement: questionObj.supplement,
                  answer: questionObj.answers[questionObj.correctIndex],
                  userCorrect: questionObj.correctIndex === this.state.userClicked,
                  user: (this.state.userClicked ? questionObj.answers[this.state.userClicked] : "INCOMPLETE"),
                 });
        if (this.state.currentQ + 1 >= (this.state.questionList.length )){
            this.props.sendResults(buf);
            this.props.history.push(this.props.match.url+"/results");
        }
        else{
            this.setState({
                currentQ: this.state.currentQ + 1,
                timedOut: false,
                userClicked: null,
                userStats: buf,
            }, this.refTimer.current.start());
        }
        this._refreshDelay = null;
    }

    _renderAnswerbuttons( answer, index ) {
        return (<QuizAnswerButton key={"answer-"+index}
                                  answerText={answer}
                                  correct={this.state.questionList[this.state.currentQ].correctIndex === index}
                                  index={index}
                                  timedOut={this.state.timedOut}
                                  userClicked={this.state.userClicked}
                                  callBackAnswer={this.callBackAnswer}/>);
    }

    render() {
        return (
            <div id="quiz" className="d-flex flex-column align-items-center">
                <QuizTimer callBackTimeOut={this.callBackTimeOut}
                           active={this.props.timed}
                           ref={this.refTimer}/>
                <div id="quiz-count"
                     className="centre title">
                    Question {(this.state.currentQ+1)+"/"+this.state.questionList.length}
                </div>
                <div id="quiz-question"
                     className="centre title">
                    {this.state.questionList[this.state.currentQ].question}
                </div>
                <div id="quiz-supplement"
                     className={this.state.timedOut || this.state.userClicked !== null ? "" : "hidden"}>
                    {this.state.questionList[this.state.currentQ].supplement}
                </div>
                <div id="quiz-body"
                     className="container ">
                    <div className="row">
                    {
                        this.state.questionList[this.state.currentQ].answers.map(this._renderAnswerbuttons) 
                    }
                    </div>
                </div>
            </div>
        );
    };
}

export default Quiz;
