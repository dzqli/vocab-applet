import React from 'react';
import '../styles/vocabtile.css'

class VocabTile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        //this.changeStage = this.changeStage.bind(this);
    };

    render() {
        return (
            <div id={"vocab-"+ (this.props.kanji ? this.props.kanji : this.props.hiragana)} 
                 className="vocab-tile"
                 onClick={()=> window.location.assign("https://jisho.org/search/"+(this.props.kanji ? this.props.kanji : this.props.hiragana))}>
                <div className="tile-overlay d-flex flex-column align-items-stretch">
                    <div className="vocab-tile-title">{this.props.kanji ? this.props.kanji : this.props.hiragana}</div>
                    <div className="vocab-tile-description">{this.props.hiragana}</div>
                    <div className="vocab-tile-description">{this.props.english}</div>
                </div>
            </div>
        );
    };
}
export default VocabTile;