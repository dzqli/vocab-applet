import React from 'react';
import { listData } from './ListData';
import ListTile from './ListTile';
import '../styles/listpage.css';
import JapaneseBG from '../assets/JapaneseBG.svg';

class ListPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lists: []
        };
        this.goToList = this.goToList.bind(this);
    };

    componentDidMount() {
        this.setState({
            lists: listData.getListNameDescription(),
        });
    } 

    goToList ( route ) {
        console.log("kek")
        this.props.history.push(this.props.match.url + route)
    }

    renderTiles() {
        var tileMarkup = [];

        for (var i = 0; i < this.state.lists.length; i++){
            tileMarkup.push(<ListTile key={i}
                                      title={this.state.lists[i].name}
                                      description={this.state.lists[i].description}
                                      BG={JapaneseBG}
                                      navFunc={this.goToList}/>);
        }
        return tileMarkup;
    }

    render() {
        return (
            <div id="list-page" className="d-flex flex-column">
                <div className="centre title">
                    Please select a Vocab List.
                </div>
                <div id="list-tile-wrapper" className="d-flex flex-wrap">
                    { this.renderTiles() }
                </div>
            </div>
        );
    };
}
export default ListPage;