import React from 'react';
import '../styles/quizsummary.css'

//potentially really bad naming here.  not an issure right now, but uh..
class QuizSummary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };

        this.renderQuestionSummary = this.renderQuestionSummary.bind(this);
    };

    renderQuestionSummary( question, index ) {
        return (
            <div className={"d-flex flex-column align-items-center quiz-question-unit col-md-5 " +  (question.userCorrect ? "correct" : "incorrect")}>
                <div id="quiz-question-question">
                    Kanji: <span className="variable">{question.question}</span>
                </div>
                <div id="quiz-question-supplement">
                    Hiragana: <span className="variable">{question.supplement}</span>
                </div>
                <div id="quiz-question-answer">
                    English: <span className="variable">{question.answer}</span>
                </div>
                <div id="quiz-question-user-answer"
                     className={question.userCorrect ? "swappedOut" : ""}>
                    User Answer: <span className="variable">{question.user}</span>
                </div>      
            </div>  
        );
    }

    _getScore(){
        return this.props.summary.reduce((total, result)=>{
            return total + ( result.userCorrect ? 1 : 0);
        }, 0);
    }

    render() {
        console.log(this.props.summary);
        return (
            <div id="quiz-summary" className="d-flex flex-column align-items-centre">
                <div className="centre title">
                    Quiz Results
                </div>
                <div id="quiz-recap-wrapper"
                     className="d-flex flex-column align-items-centre">
                    <div id="quiz-report-score"
                         className="centre">
                        Score: <span className="variable">{this._getScore() + "/" + this.props.summary.length}</span>
                    </div>
                    <div id="quiz-report-style"
                         className="centre">
                        Style: <span className="variable">{this.props.isTimed ? "Timed" : "Not Timed" }</span>
                    </div>
                </div>
                <div id="quiz-details"
                     className="d-flex flex-stretch flex-wrap">
                    {this.props.summary.map(this.renderQuestionSummary)}
                </div>
            </div>
        );
    };
}

export default QuizSummary;
